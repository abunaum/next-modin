import useAuth from "./auth";
import { useRouter } from "next/router";
import { Box, CircularProgress } from "@mui/material";

export function withPublic(Component) {
  return function WithPublic(props) {
    const auth = useAuth();
    const router = useRouter();

    if (!auth.user) {
      return <Component auth={auth} {...props} />;
    } else {
      router.replace("/");
      return (
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          minHeight="100vh"
        >
          <CircularProgress />
        </Box>
      );
    }
  };
}

export function withProtected(Component) {
  return function WithProtected(props) {
    const auth = useAuth();
    const router = useRouter();

    if (!auth.user) {
      router.replace("/auth/login");
      return (
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          minHeight="100vh"
        >
          <CircularProgress />
        </Box>
      );
    } else {
      return <Component auth={auth} {...props} />;
    }
  };
}

export function adminArea(Component) {
  return function AdminArea(props) {
    const auth = useAuth();
    const router = useRouter();

    if (!auth.user) {
      router.replace("/auth/login");
      return (
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          minHeight="100vh"
        >
          <CircularProgress />
        </Box>
      );
    } else {
      if (auth.user.role !== "Admin") {
        router.replace("/");
        return (
          <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            minHeight="100vh"
          >
            <CircularProgress />
          </Box>
        );
      } else {
        return <Component auth={auth} {...props} />;
      }
    }
  };
}
