import { createContext, useContext, useState } from "react";
import authServices from "../../utils/authServices";
import Swal from "sweetalert2";

const authcontext = createContext();

export default function useAuth() {
  return useContext(authcontext);
}

export function AuthProvider(props) {
  const [user, setUser] = useState(null);
  const [error, setError] = useState("");

  const loginWithgoogle = async () => {
    const { error, user } = await authServices.loginWithgoogle();
    setUser(user ?? null);
    setError(error ?? "");
    if (error) {
      Swal.fire({
        title: "Oooops!",
        html: error,
        icon: "error",
      });
    }
  };

  const logout = async () => {
    authServices.logout();
    setUser(null);
  };

  const value = { user, error, loginWithgoogle, logout, setUser };

  return <authcontext.Provider value={value} {...props} />;
}
