// ** Custom Menu Components
import VerticalNavLink from "./VerticalNavLink";
import VerticalNavSectionTitle from "./VerticalNavSectionTitle";
import useAuth from "../../../../../context/auth";

const resolveNavItemComponent = (item) => {
  if (item.sectionTitle) {
    return VerticalNavSectionTitle;
  } else {
    return VerticalNavLink;
  }
};

const VerticalNavItems = (props) => {
  const { user } = useAuth();
  // ** Props
  const { verticalNavItems } = props;

  const RenderMenuItems = verticalNavItems?.map((item, index) => {
    const TagName = resolveNavItemComponent(item);
    if (user?.role === "Admin") {
      return <TagName {...props} key={index} item={item} />;
    } else {
      if (item.type !== "admin") {
        return <TagName {...props} key={index} item={item} />;
      }
    }
  });

  return <>{RenderMenuItems}</>;
};

export default VerticalNavItems;
