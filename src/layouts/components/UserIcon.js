import { Icon } from "@iconify/react";

const UserIcon = (props) => {
  const { icon } = props;
  return <Icon icon={icon} width="24" height="24" />;
};

export default UserIcon;
