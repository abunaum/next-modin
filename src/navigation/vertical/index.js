// ** Icon imports
// import Login from "mdi-material-ui/Login";
// import Table from "mdi-material-ui/Table";
// import CubeOutline from "mdi-material-ui/CubeOutline";
// import HomeOutline from "mdi-material-ui/HomeOutline";
// import FormatLetterCase from "mdi-material-ui/FormatLetterCase";
// import CreditCardOutline from "mdi-material-ui/CreditCardOutline";
// import GoogleCirclesExtended from "mdi-material-ui/GoogleCirclesExtended";
import { Icon } from "@iconify/react";

const menu = [
  {
    type: "basic",
    title: "Dashboard",
    icon: "bx:home-heart",
    path: "/",
  },
  {
    type: "basic",
    sectionTitle: "Nikah",
  },
  {
    type: "basic",
    title: "Nikah Masuk",
    icon: "akar-icons:cloud-download",
    path: "/auth/login",
    // openInNewTab: true
  },
  {
    type: "basic",
    sectionTitle: "Person",
  },
  {
    type: "basic",
    title: "Person",
    icon: "fa6-solid:user-group",
    path: "/person",
  },
  {
    type: "basic",
    title: "Tambah Person",
    icon: "bi:person-plus-fill",
    path: "/person/tambah",
  },
  {
    type: "basic",
    sectionTitle: "Setting",
  },
  {
    type: "basic",
    title: "Modin",
    icon: "ant-design:setting-filled",
    path: "/typography",
  },
  {
    type: "admin",
    sectionTitle: "Admin",
  },
  {
    type: "admin",
    title: "User",
    icon: "carbon:user-avatar-filled-alt",
    path: "/user",
    // openInNewTab: true
  },
];

const navigation = () => {
  return menu;
};

export default navigation;
