const sortlokasi = async (data) => {
  const newdata = await data.sort(function (a, b) {
    return a.nama.localeCompare(b.nama);
  });
  return newdata;
};

export default sortlokasi;
