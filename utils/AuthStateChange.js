import React, { useEffect, useState } from "react";
import { getAuth } from "firebase/auth";
import useAuth from "../src/context/auth";
import { initFirebase, db } from "./firebase";
import { doc, onSnapshot } from "firebase/firestore";
import CircularProgress from "@mui/material/CircularProgress";
import { Box } from "@mui/material";
export default function AuthStateChange({ children }) {
  const app = initFirebase();
  const auth = getAuth();
  const { setUser } = useAuth();
  const [loading, setLoading] = useState(true);
  const dbnya = db();
  useEffect(() => {
    auth.onAuthStateChanged(async (user) => {
      if (user) {
        onSnapshot(doc(dbnya, "User", user.email), (doc) => {
          if (doc.data()) {
            setUser(doc.data());
            setLoading(false);
          } else {
            setUser(null);
            setLoading(false);
          }
        });
      } else {
        setUser(user);
        setLoading(false);
      }
    });
  }, []);

  if (loading) {
    return (
      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        minHeight="100vh"
      >
        <CircularProgress />
      </Box>
    );
  }

  return children;
}
