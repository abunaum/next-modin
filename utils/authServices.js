import { initFirebase, db } from "./firebase";
import {
  getAuth,
  signOut,
  signInWithPopup,
  GoogleAuthProvider,
  GithubAuthProvider,
} from "firebase/auth";
import { doc, getDoc, updateDoc, serverTimestamp } from "firebase/firestore";

export const authServices = {
  loginWithgoogle: async () => {
    const app = initFirebase();
    const auth = getAuth();
    const provider = new GoogleAuthProvider();
    const dbnya = db();
    try {
      const usrcrd = await signInWithPopup(auth, provider);
      const userdata = await doc(dbnya, "User", usrcrd.user.email);
      const cekuser = await getDoc(userdata);
      if (cekuser.exists()) {
        const setnewdata = doc(dbnya, "User", usrcrd.user.email);
        const newdata = {
          email: usrcrd.user.email,
          gambar: usrcrd.user.photoURL,
          nama: usrcrd.user.displayName,
          last_login: serverTimestamp(),
        };
        const UD = await updateDoc(setnewdata, newdata);
        return {
          user: UD,
        };
      } else {
        await signOut(auth);
        return {
          error: "User tidak terdaftar",
        };
      }
    } catch (e) {
      console.log(e);
      return {
        error: "Gagal login google !!!",
      };
    }
  },
  logout: async () => {
    const app = initFirebase();
    const auth = getAuth();
    await signOut(auth);
  },
};

export default authServices;
