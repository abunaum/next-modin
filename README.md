## Requirements

yarn v3
nodejs v18^
npm v8^
opener

## Enable yarn

```bash
corepack enable
```

## Install package

```bash
npm i -g opener
```

# then

```bash
yarn
```

## Start app

```bash
yarn dev
```

And wait for browser open then refresh browser page
