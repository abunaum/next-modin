// ** MUI Imports
import Grid from "@mui/material/Grid";

// ** Icons Imports
import Poll from "mdi-material-ui/Poll";
import CurrencyUsd from "mdi-material-ui/CurrencyUsd";
import HelpCircleOutline from "mdi-material-ui/HelpCircleOutline";
import BriefcaseVariantOutline from "mdi-material-ui/BriefcaseVariantOutline";

// ** Custom Components Imports
import CardStatisticsVerticalComponent from "../src/@core/components/card-statistics/card-stats-vertical";

// ** Styled Component Import
import ApexChartWrapper from "../src/@core/styles/libs/react-apexcharts";

// ** Demo Components Imports
import { withProtected } from "../src/context/protect";

const Dashboard = ({ auth }) => {
  return (
    <ApexChartWrapper>
      <Grid container spacing={12}>
        <Grid item xs={6}>
          <CardStatisticsVerticalComponent
            stats="$25.6k"
            icon={<Poll />}
            color="success"
            trendNumber="+42%"
            title="Total Profit"
            subtitle="Weekly Profit"
          />
        </Grid>
        <Grid item xs={6}>
          <CardStatisticsVerticalComponent
            stats="$78"
            title="Refunds"
            trend="negative"
            color="secondary"
            trendNumber="-15%"
            subtitle="Past Month"
            icon={<CurrencyUsd />}
          />
        </Grid>
        <Grid item xs={6}>
          <CardStatisticsVerticalComponent
            stats="862"
            trend="negative"
            trendNumber="-18%"
            title="New Project"
            subtitle="Yearly Project"
            icon={<BriefcaseVariantOutline />}
          />
        </Grid>
        <Grid item xs={6}>
          <CardStatisticsVerticalComponent
            stats="15"
            color="warning"
            trend="negative"
            trendNumber="-18%"
            subtitle="Last Week"
            title="Sales Queries"
            icon={<HelpCircleOutline />}
          />
        </Grid>
      </Grid>
    </ApexChartWrapper>
  );
};

export default withProtected(Dashboard);
