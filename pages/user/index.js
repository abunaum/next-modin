import { useEffect, useState } from "react";

// ** MUI Imports
import {
  Avatar,
  Button,
  Paper,
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TextField,
  InputAdornment,
  Grid,
  Card,
  CardHeader,
} from "@mui/material";
import Magnify from "mdi-material-ui/Magnify";

// ** Demo Components Imports
import { adminArea } from "../../src/context/protect";

// Firebase
import { db } from "../../utils/firebase";
import {
  doc,
  deleteDoc,
  setDoc,
  collection,
  onSnapshot,
} from "firebase/firestore";

// swal
import Swal from "sweetalert2";

const hapususer = (email) => {
  Swal.fire({
    title: "Anda yakin?",
    text: "Mau menghapus " + email + " ?",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Gass Hapus",
    cancelButtonText: "Gak jadi",
  }).then(async (result) => {
    if (result.isConfirmed) {
      const dbnya = db();
      await deleteDoc(doc(dbnya, "User", email));
      Swal.fire({
        title: "Mantap!",
        html: email + " berhasil di hapus.",
        timer: 3000,
        timerProgressBar: true,
        icon: "success",
        showConfirmButton: false,
      });
    }
  });
};

const tambahuser = () => {
  Swal.fire({
    title: "Masukkan Email",
    input: "email",
    inputAttributes: {
      autocapitalize: "off",
    },
    showCancelButton: true,
    confirmButtonText: "Tambah",
    cancelButtonText: "Batal",
    showLoaderOnConfirm: true,
    preConfirm: async (email) => {
      const dbnya = db();
      const tambah = await setDoc(doc(dbnya, "User", email), {
        email: email,
        role: "Modin",
      });
      return tambah;
    },
    allowOutsideClick: () => !Swal.isLoading(),
  }).then((result) => {
    if (result.isConfirmed) {
      Swal.fire({
        title: "Mantap!",
        html: result.value + " berhasil di tambah.",
        timer: 3000,
        timerProgressBar: true,
        icon: "success",
        showConfirmButton: false,
      });
    }
  });
};

const User = ({ auth }) => {
  // ** States
  const [user, setUser] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const dbnya = db();
  function alldata() {
    onSnapshot(collection(dbnya, "User"), (querySnapshot) => {
      const usr = [];
      querySnapshot.forEach(async (doc) => {
        const cdt = await createData(
          doc.data().nama ?? "Belum Login",
          doc.data().email,
          doc.data().gambar ?? "",
          doc.data().role
        );
        usr.push(cdt);
      });
      setUser(usr);
    });
  }

  const caridata = (keyword) => {
    const key = keyword.toLowerCase();
    onSnapshot(collection(dbnya, "User"), (querySnapshot) => {
      const usr = [];
      querySnapshot.forEach(async (doc) => {
        const email = doc.data().email;
        const nama = doc.data().nama;
        const role = doc.data().role;
        if (
          email.toLowerCase().includes(key) ||
          nama.toLowerCase().includes(key) ||
          role.toLowerCase().includes(key)
        ) {
          const cdt = await createData(
            nama ?? "Belum Login",
            email,
            doc.data().gambar ?? "",
            role
          );
          usr.push(cdt);
        }
      });
      setUser(usr);
    });
  };

  useEffect(() => {
    alldata();
  }, []);

  const columns = [
    { id: "nama", label: "Nama", minWidth: 170 },
    { id: "email", label: "Email", minWidth: 100 },
    { id: "gambar", label: "Gambar", minWidth: 170 },
    { id: "role", label: "Role", minWidth: 170 },
    { id: "aksi", label: "Aksi", minWidth: 170 },
  ];

  function createData(nama, email, gambar, role) {
    var aksi = <></>;
    if (email !== auth.user.email) {
      aksi = (
        <Button variant="contained" onClick={(e) => hapususer(email)}>
          Hapus
        </Button>
      );
    }
    const avatar = (
      <a href={gambar} target="_blank" rel="noreferrer">
        <Avatar alt={nama} sx={{ width: 40, height: 40 }} src={gambar} />
      </a>
    );
    return { nama, email, gambar: avatar, role, aksi };
  }
  const qr = (e) => {
    if (e !== "") {
      caridata(e);
    } else {
      alldata();
    }
  };
  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <TextField
          size="small"
          sx={{
            "& .MuiOutlinedInput-root": { borderRadius: 4 },
            mb: 3,
            width: { xs: "90vw", md: "75vw" },
          }}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Magnify fontSize="small" />
              </InputAdornment>
            ),
          }}
          label="cari data"
          onChange={(e) => qr(e.target.value)}
        />
        <Card>
          <CardHeader
            title="Data User"
            titleTypographyProps={{ variant: "h6" }}
          />
          <Paper sx={{ width: "100%", overflow: "hidden" }}>
            <TableContainer sx={{ maxHeight: 440 }}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    {columns.map((column) => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        sx={{ minWidth: column.minWidth }}
                      >
                        {column.label}
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {user
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => {
                      return (
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                          key={row.code}
                        >
                          {columns.map((column) => {
                            const value = row[column.id];

                            return (
                              <TableCell key={column.id} align={column.align}>
                                {column.format && typeof value === "number"
                                  ? column.format(value)
                                  : value}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={user.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Paper>
          <center>
            <Button
              variant="contained"
              sx={{ mt: 3, mb: 3 }}
              onClick={tambahuser}
            >
              Tambah User
            </Button>
          </center>
        </Card>
      </Grid>
    </Grid>
  );
};

export default adminArea(User);
