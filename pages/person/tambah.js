import { useEffect, useState } from "react";

// ** MUI Imports
import {
  Grid,
  Card,
  Button,
  Divider,
  MenuItem,
  TextField,
  CardHeader,
  InputLabel,
  IconButton,
  Typography,
  CardContent,
  CardActions,
  FormControl,
  OutlinedInput,
  InputAdornment,
  Select,
} from "@mui/material";

// ** Third Party Imports
import DatePicker from "react-datepicker";
import Stack from "@mui/material/Stack";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { MobileDatePicker } from "@mui/x-date-pickers/MobileDatePicker";

// ** Icons Imports
import EyeOutline from "mdi-material-ui/EyeOutline";
import EyeOffOutline from "mdi-material-ui/EyeOffOutline";

// ** Styled Component
import DatePickerWrapper from "../../src/@core/styles/libs/react-datepicker";

// ** Demo Components Imports
import { withProtected } from "../../src/context/protect";

// Firebase
import { db } from "../../utils/firebase";
import { setDoc, doc, getDoc, deleteDoc, Timestamp } from "firebase/firestore";

// swal
import Swal from "sweetalert2";

// axios
import axios from "axios";

import sortlokasi from "../../utils/sortlokasi";

const Tambah = ({ auth }) => {
  const defaultpilihan = [{ id: "", nama: "Pilih" }];
  // ** States
  const [date, setDate] = useState(Date());
  const [rtrw, setRtrw] = useState(null);
  const [prov, setProv] = useState([]);
  const [kabkot, setKabkot] = useState(defaultpilihan);
  const [kec, setKec] = useState(defaultpilihan);
  const [keldes, setKeldes] = useState(defaultpilihan);
  const [lengkap, setLengkap] = useState(false);

  useEffect(() => {
    loadprov();
  }, []);

  async function loadprov() {
    const allprov = await axios.get(
      "https://cdn.jsdelivr.net/gh/abunaum/lokasi@main/allprov.json"
    );
    const data = await allprov.data;
    const fixdata = await sortlokasi(data);
    setLengkap(false);
    setProv(fixdata);
    setKabkot(defaultpilihan);
    setKec(defaultpilihan);
    setKeldes(defaultpilihan);
  }

  async function loadkabupaten(prov) {
    setLengkap(false);
    Swal.fire({
      title: "Loading Data",
      html: "Mencari data Kabupaten / Kota<br>di Provinsi<br>" + prov[1],
      showConfirmButton: false,
      didOpen: () => {
        Swal.showLoading();
      },
      allowOutsideClick: false,
      allowEscapeKey: false,
      allowEnterKey: false,
    });
    async function cekkabkot(prov) {
      const splitprov = prov.split("-");
      const idprov = splitprov[0];
      const kabupaten = await axios.get(
        `https://cdn.jsdelivr.net/gh/abunaum/lokasi@main/provinsi/${idprov}.json`
      );
      const data = await kabupaten.data;
      const fixdata = await sortlokasi(data);
      setKabkot(fixdata);
      setKec(defaultpilihan);
      setKeldes(defaultpilihan);
    }
    await cekkabkot(prov);
    Swal.close();
  }
  async function loadkecamatan(kabkot) {
    setLengkap(false);
    Swal.fire({
      title: "Loading Data",
      html: "Mencari data Kabupaten / Kota<br>di Provinsi<br>" + prov[1],
      showConfirmButton: false,
      didOpen: () => {
        Swal.showLoading();
      },
      allowOutsideClick: false,
      allowEscapeKey: false,
      allowEnterKey: false,
    });
    async function cekkec(kabkot) {
      const splitkabkot = kabkot.split("-");
      const idkabkot = splitkabkot[0];
      const kec = await axios.get(
        `https://cdn.jsdelivr.net/gh/abunaum/lokasi@main/kabupaten/${idkabkot}.json`
      );
      const data = await kec.data;
      const fixdata = await sortlokasi(data);
      setKec(fixdata);
      setKeldes(defaultpilihan);
    }
    await cekkec(kabkot);
    Swal.close();
  }
  async function loadkeldes(kec) {
    setLengkap(false);
    Swal.fire({
      title: "Loading Data",
      html: "Mencari data Kabupaten / Kota<br>di Provinsi<br>" + prov[1],
      showConfirmButton: false,
      didOpen: () => {
        Swal.showLoading();
      },
      allowOutsideClick: false,
      allowEscapeKey: false,
      allowEnterKey: false,
    });
    async function cekkeldes(kec) {
      const splitkec = kec.split("-");
      const idkec = splitkec[0];
      const keldes = await axios.get(
        `https://cdn.jsdelivr.net/gh/abunaum/lokasi@main/kecamatan/${idkec}.json`
      );
      const data = await keldes.data;
      const fixdata = await sortlokasi(data);
      setKeldes(fixdata);
    }

    await cekkeldes(kec);
    Swal.close();
  }

  const handletgllahir = (newValue) => {
    setDate(newValue);
  };
  const dbnya = db();
  async function tambahperson(data) {
    try {
      await setDoc(doc(dbnya, "Person", data.nik.toString()), data);
      return Swal.fire({
        title: "Mantap!",
        html: "Data berhasil di tambah",
        timer: 3000,
        timerProgressBar: true,
        icon: "success",
        showConfirmButton: false,
      });
    } catch (error) {
      console.log(error);
      return Swal.fire({
        title: "Ooops!",
        html: "Gagal menambah data",
        timer: 3000,
        timerProgressBar: true,
        icon: "error",
        showConfirmButton: false,
      });
    }
  }
  async function submit(e) {
    e.preventDefault();
    var data = new FormData(e.target);
    let formdata = Object.fromEntries(data.entries());
    const nik = formdata.nik;
    const loadperson = doc(dbnya, "Person", nik);
    const cariperson = await getDoc(loadperson);
    if (lengkap) {
      if (cariperson.exists()) {
        Swal.fire({
          title: "Ooops!",
          html: "NIK '" + cariperson.data().nik + "' sudah ada.",
          timer: 3000,
          timerProgressBar: true,
          icon: "error",
          showConfirmButton: false,
        });
      } else {
        // doc.data() will be undefined in this case
        const tgl_lahir = Timestamp.fromDate(new Date(date));
        formdata = Object.assign({ tanggal_lahir: tgl_lahir }, formdata);
        const prov = formdata.prov.split("-");
        const kabkot = formdata.kabkot.split("-");
        const kec = formdata.kec.split("-");
        formdata.prov = prov[1];
        formdata.nik = parseInt(formdata.nik);
        formdata.nama = formdata.nama.toLocaleUpperCase();
        formdata.kabkot = kabkot[1];
        formdata.kec = kec[1];
        console.log(formdata);
        await tambahperson(formdata);
      }
    } else {
      Swal.fire({
        title: "Ooops!",
        html: "Data alamat belum lengkap.",
        timer: 3000,
        timerProgressBar: true,
        icon: "error",
        showConfirmButton: false,
      });
    }
  }
  return (
    <DatePickerWrapper>
      <Grid container spacing={6}>
        <Grid item xs={12}>
          <Card>
            <CardHeader
              title="Tambah Person"
              titleTypographyProps={{ variant: "h6" }}
            />
            <Divider sx={{ margin: 0 }} />
            <form onSubmit={(e) => submit(e)}>
              <CardContent>
                <Grid container spacing={5}>
                  <Grid item xs={12}>
                    <Typography variant="body2" sx={{ fontWeight: 600 }}>
                      1. Data Person
                    </Typography>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      fullWidth
                      label="Nama"
                      placeholder="Nama"
                      name="nama"
                      required
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      fullWidth
                      type="number"
                      label="NIK"
                      name="nik"
                      required
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <FormControl fullWidth>
                      <InputLabel id="form-layouts-separator-select-label">
                        Jenis Kelamin
                      </InputLabel>
                      <Select
                        label="Jenis Kelamin"
                        defaultValue=""
                        id="form-layouts-separator-select"
                        labelId="form-layouts-separator-select-label"
                        name="jk"
                        required
                      >
                        <MenuItem value="lk">Laki - Laki</MenuItem>
                        <MenuItem value="pr">Perempuan</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      fullWidth
                      label="Pekerjaan"
                      placeholder="Pekerjaan"
                      defaultValue="Wiraswasta"
                      name="pekerjaan"
                      required
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      fullWidth
                      label="Tempat Lahir"
                      placeholder="Tempat Lahir"
                      name="tempat_lahir"
                      defaultValue="Probolinggo"
                      required
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <Stack spacing={3}>
                        <MobileDatePicker
                          label="Tanggal Lahir"
                          inputFormat="MM/DD/YYYY"
                          value={date}
                          onChange={handletgllahir}
                          renderInput={(params) => <TextField {...params} />}
                          name="tanggal_lahir"
                          required
                        />
                      </Stack>
                    </LocalizationProvider>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      fullWidth
                      label="Agama"
                      placeholder="Agama"
                      defaultValue="Islam"
                      name="agama"
                      required
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      fullWidth
                      label="Kewarganegaraan"
                      placeholder="Kewarganegaraan"
                      defaultValue="WNI"
                      name="wn"
                      required
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Divider sx={{ marginBottom: 0 }} />
                  </Grid>
                  <Grid item xs={12}>
                    <Typography variant="body2" sx={{ fontWeight: 600 }}>
                      2. Alamat
                    </Typography>
                  </Grid>
                  <Grid item xs={6} sm={3}>
                    <FormControl fullWidth>
                      <InputLabel id="form-layouts-separator-select-label">
                        Provinsi
                      </InputLabel>
                      <Select
                        label="Provinsi"
                        defaultValue=""
                        id="form-layouts-separator-select"
                        labelId="form-layouts-separator-select-label"
                        name="prov"
                        onChange={(e) => loadkabupaten(e.target.value)}
                        required
                      >
                        {prov.map((data, index) => (
                          <MenuItem
                            value={data.id + "-" + data.nama}
                            key={index}
                          >
                            {data.nama}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={6} sm={3}>
                    <FormControl fullWidth>
                      <InputLabel id="form-layouts-separator-select-label">
                        Kabupaten / Kota
                      </InputLabel>
                      <Select
                        label="Kabupaten / Kota"
                        defaultValue=""
                        id="form-layouts-separator-select"
                        labelId="form-layouts-separator-select-label"
                        onChange={(e) => loadkecamatan(e.target.value)}
                        name="kabkot"
                        required
                      >
                        {kabkot.map((data, index) => (
                          <MenuItem
                            value={data.id + "-" + data.nama}
                            key={index}
                          >
                            {data.nama}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={6} sm={3}>
                    <FormControl fullWidth>
                      <InputLabel id="form-layouts-separator-select-label">
                        Kecamatan
                      </InputLabel>
                      <Select
                        label="Kecamatan"
                        defaultValue=""
                        id="form-layouts-separator-select"
                        labelId="form-layouts-separator-select-label"
                        onChange={(e) => loadkeldes(e.target.value)}
                        name="kec"
                        required
                      >
                        {kec.map((data, index) => (
                          <MenuItem
                            value={data.id + "-" + data.nama}
                            key={index}
                          >
                            {data.nama}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={6} sm={3}>
                    <FormControl fullWidth>
                      <InputLabel id="form-layouts-separator-select-label">
                        Desa / Kelurahan
                      </InputLabel>
                      <Select
                        label="Desa / Kelurahan"
                        defaultValue=""
                        id="form-layouts-separator-select"
                        labelId="form-layouts-separator-select-label"
                        name="keldes"
                        onChange={(e) => setLengkap(true)}
                        required
                      >
                        {keldes.map((data, index) => (
                          <MenuItem value={data.nama} key={index}>
                            {data.nama}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      fullWidth
                      label="Dusun / Jalan"
                      placeholder="Dusun / Jalan"
                      name="jalan"
                      required
                    />
                  </Grid>
                  <Grid item xs={6} sm={3}>
                    <TextField
                      fullWidth
                      type="number"
                      label="RT"
                      name="rt"
                      required
                    />
                  </Grid>
                  <Grid item xs={6} sm={3}>
                    <TextField
                      fullWidth
                      type="number"
                      label="RW"
                      name="rw"
                      required
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <Divider sx={{ margin: 0 }} />
              <CardActions>
                <Button
                  size="large"
                  type="submit"
                  sx={{ mr: 2 }}
                  variant="contained"
                >
                  Simpan
                </Button>
              </CardActions>
            </form>
          </Card>
        </Grid>
      </Grid>
    </DatePickerWrapper>
  );
};

export default withProtected(Tambah);
