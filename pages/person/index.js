import { useEffect, useState } from "react";
import Link from "next/link";

// ** MUI Imports
import {
  Avatar,
  Button,
  Paper,
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TextField,
  InputAdornment,
  Grid,
  Card,
  CardHeader,
} from "@mui/material";
import Magnify from "mdi-material-ui/Magnify";

// ** Demo Components Imports
import { withProtected } from "../../src/context/protect";

// Firebase
import { db } from "../../utils/firebase";
import { doc, deleteDoc, collection, onSnapshot } from "firebase/firestore";

// swal
import Swal from "sweetalert2";

//moments
import moment from "moment";
import "moment/locale/id"; // without this line it didn't work
moment.locale("id");

const hapusperson = (nik) => {
  Swal.fire({
    title: "Anda yakin?",
    text: "Mau menghapus " + nik + " ?",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Gass Hapus",
    cancelButtonText: "Gak jadi",
  }).then(async (result) => {
    if (result.isConfirmed) {
      const dbnya = db();
      try {
        await deleteDoc(doc(dbnya, "Person", nik.toString()));
        Swal.fire({
          title: "Mantap!",
          html: nik + " berhasil di hapus.",
          timer: 3000,
          timerProgressBar: true,
          icon: "success",
          showConfirmButton: false,
        });
      } catch (error) {
        console.log(error);
        Swal.fire({
          title: "Oooops!",
          html: nik + " gagal di hapus.",
          timer: 3000,
          timerProgressBar: true,
          icon: "error",
          showConfirmButton: false,
        });
      }
    }
  });
};

const Person = ({ auth }) => {
  // ** States
  const [person, setPerson] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const dbnya = db();
  function alldata() {
    onSnapshot(collection(dbnya, "Person"), (querySnapshot) => {
      const prsn = [];
      querySnapshot.forEach(async (doc) => {
        const tgl = moment
          .unix(doc.data().tanggal_lahir.seconds)
          .format("DD MMMM YYYY");
        const cdt = await createData(
          doc.data().nik,
          doc.data().nama,
          doc.data().jk,
          doc.data().tempat_lahir,
          tgl,
          doc.data().agama,
          doc.data().jalan,
          doc.data().kabkot,
          doc.data().kec,
          doc.data().keldes,
          doc.data().pekerjaan,
          doc.data().prov,
          doc.data().rt,
          doc.data().rw,
          doc.data().wn
        );
        prsn.push(cdt);
      });
      setPerson(prsn);
    });
  }

  const caridata = (keyword) => {
    const key = keyword.toLowerCase();
    onSnapshot(collection(dbnya, "Person"), (querySnapshot) => {
      const prsn = [];
      querySnapshot.forEach(async (doc) => {
        const nik = doc.data().nik;
        const nama = doc.data().nama;
        const tempat_lahir = doc.data().tempat_lahir;
        const jalan = doc.data().jalan;
        const prov = doc.data().prov;
        const kabkot = doc.data().kabkot;
        const kec = doc.data().kec;
        const keldes = doc.data().keldes;
        const rt = doc.data().rt;
        const rw = doc.data().rw;
        const alamat = `${jalan}, RT ${rt} RW ${rw}`;
        const tanggal_lahir = doc.data().tanggal_lahir.seconds;
        const tgl = moment.unix(tanggal_lahir).format("DD MMMM YYYY");
        const ttl = tempat_lahir + ", " + tanggal_lahir;
        if (
          nik.toString().includes(key) ||
          nama.toLowerCase().includes(key) ||
          ttl.toLowerCase().includes(key) ||
          prov.toLowerCase().includes(key) ||
          kabkot.toLowerCase().includes(key) ||
          kec.toLowerCase().includes(key) ||
          keldes.toLowerCase().includes(key) ||
          alamat.toLowerCase().includes(key)
        ) {
          const cdt = await createData(
            nik,
            nama,
            doc.data().jk,
            tempat_lahir,
            tgl,
            doc.data().agama,
            jalan,
            kabkot,
            kec,
            keldes,
            doc.data().pekerjaan,
            prov,
            rt,
            rw,
            doc.data().wn
          );
          prsn.push(cdt);
        }
      });
      setPerson(prsn);
    });
  };

  useEffect(() => {
    alldata();
  }, []);

  const columns = [
    { id: "nik", label: "Nik", minWidth: 170 },
    { id: "nama", label: "Nama", minWidth: 250 },
    { id: "jenis_kelamin", label: "Jenis Kelamin", minWidth: 170 },
    { id: "ttl", label: "TTL", minWidth: 250 },
    { id: "pekerjaan", label: "Pekerjaan", minWidth: 170 },
    { id: "alamat", label: "Alamat", minWidth: 300 },
    { id: "keldes", label: "Kelurahan / Desa", minWidth: 170 },
    { id: "kec", label: "Kecamatan", minWidth: 170 },
    { id: "kabkot", label: "Kota / Kabupaten", minWidth: 170 },
    { id: "prov", label: "Provinsi", minWidth: 170 },
    { id: "agama", label: "Agama", minWidth: 170 },
    { id: "wn", label: "Kewarganegaraan", minWidth: 170 },
    { id: "aksi", label: "Aksi", minWidth: 170 },
  ];

  function createData(
    nik,
    nama,
    jk,
    tempat_lahir,
    tanggal_lahir,
    agama,
    jalan,
    kabkot,
    kec,
    keldes,
    pekerjaan,
    prov,
    rt,
    rw,
    wn
  ) {
    var alamat = `${jalan}, RT ${rt} RW ${rw}`;
    var aksi = (
      <>
        <Button variant="contained" onClick={(e) => hapusperson(nik)}>
          Hapus
        </Button>
      </>
    );
    var jenis_kelamin = "Laki - laki";
    if (jk !== "lk") {
      jenis_kelamin = "Perempuan";
    }
    return {
      nik,
      nama,
      jenis_kelamin,
      ttl: tempat_lahir + ", " + tanggal_lahir,
      agama,
      kabkot,
      kec,
      keldes,
      pekerjaan,
      prov,
      alamat,
      wn,
      aksi,
    };
  }
  const qr = (e) => {
    if (e !== "") {
      caridata(e);
    } else {
      alldata();
    }
  };
  // console.log(person);
  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <TextField
          size="small"
          sx={{
            "& .MuiOutlinedInput-root": { borderRadius: 4 },
            mb: 3,
            width: { xs: "90vw", md: "75vw" },
          }}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Magnify fontSize="small" />
              </InputAdornment>
            ),
          }}
          label="cari data"
          onChange={(e) => qr(e.target.value)}
        />
        <Card>
          <CardHeader
            title="Data Person"
            titleTypographyProps={{ variant: "h6" }}
          />
          <Paper sx={{ width: "100%", overflow: "hidden" }}>
            <TableContainer sx={{ maxHeight: 440 }}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    {columns.map((column) => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        sx={{ minWidth: column.minWidth }}
                      >
                        {column.label}
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {person
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => {
                      return (
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                          key={row.code}
                        >
                          {columns.map((column) => {
                            const value = row[column.id];

                            return (
                              <TableCell key={column.id} align={column.align}>
                                {column.format && typeof value === "number"
                                  ? column.format(value)
                                  : value}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={person.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Paper>
          <center>
            <Link href="/person/tambah">
              <a>
                <Button variant="contained" sx={{ mt: 3, mb: 3 }}>
                  Tambah Person
                </Button>
              </a>
            </Link>
          </center>
        </Card>
      </Grid>
    </Grid>
  );
};

export default withProtected(Person);
